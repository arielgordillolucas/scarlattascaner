import React from 'react';
import {Text, Platform} from 'react-native';

export default function CustomText(props) {
  return (
    <Text
      {...props}
      allowFontScaling={false}
      numberOfLines={props.numberLines}
      style={[ getNameFont(props.font), props.others ]}>
    </Text>
  );
}

function getNameFont(font) {
  let nameFont;
  let weight 	 = '';

  switch (font) {
    case 'bold':
      nameFont = Platform.OS === 'ios' ? 'Roboto-Bold' : 'RobotoBold';
      break;

    case 'medium':
      nameFont = Platform.OS === 'ios' ? 'Roboto-Medium' : 'RobotoMedium';
      break;

    case 'light':
      nameFont = Platform.OS === 'ios' ? 'Roboto-Light' : 'RobotoLight';
      break;

    default:
      nameFont = Platform.OS === 'ios' ? 'Roboto-Regular' : 'RobotoRegular';
      weight   = 'normal';
      break;
  }
  return { fontFamily: nameFont, }
}
