import React from 'react';
import Modal from 'react-native-modal';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import CustomText from './CustomText';
import Colors from '../constants/Colors';

export default function InternetModal(props) {
  return (
    <Modal isVisible={props.isVisible}
           deviceHeight={props.deviceHeight}
           deviceWidth={props.deviceWidth}
           animationType={'slide'}
           animationInTiming={600}
    >
      <View style={styles.modalContainer}>
        <View style={styles.modal}>
          <CustomText others={styles.modalTitle}>Aviso</CustomText>
          <CustomText others={styles.modalText}>
            Por favor, verifique su conexión a internet y trate nuevamente.
          </CustomText>
          <TouchableOpacity style={styles.button}>
            <CustomText others={styles.buttonText}>Volver a intentar</CustomText>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'center',
  },
  modalContainer: {
    backgroundColor: Colors.colorBlanco,
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  modalTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    color: Colors.colorPlomo,
  },
  modalText: {
    fontSize: 18,
    color: Colors.colorPlomo,
    marginTop: 14,
    textAlign: 'center',
    marginBottom: 10,
  },
  button: {
    alignSelf: 'center',
    backgroundColor: Colors.colorVerde,
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 10,
    width: '100%',
    alignItems: 'center',
    marginTop: 10,
  },
  buttonText: {
    color: Colors.colorBlanco,
    fontSize: 20,
  },
});
