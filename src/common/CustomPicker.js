import React, { useState } from 'react';
import {Picker} from '@react-native-picker/picker';
import { StyleSheet } from 'react-native';
import Colors from '../constants/Colors';

export default function CustomPicker(props) {
  return (
    <Picker
      onBlur={props.onBlur}
      style={props.others ?? styles.pickerStyle}
      selectedValue={props.selectedValue}
      onValueChange={props.onValueChange}
      placeholder={props.placeholder ?? 'Seleccione'}
    >
      {
        props.items.map((item) => {
          return (
            <Picker.Item
              key={item.key}
              label={item.label}
              value={item.value}
              style={styles.pickerItemStyle}
            />
          );
        })
      }
    </Picker>
  );
}

const styles = StyleSheet.create({
  pickerStyle: {

  },
  pickerItemStyle: {

  }
});
