import Colors from '../constants/Colors';
import React, { useState } from 'react';
import { TextInput, View, Text } from 'react-native';

export default function CustomNumericInput(props) {
    const handleTextChange = (text) => {
        // Solo permitir el ingreso de números
        const formattedText = text.replace(/[^0-9]/g, '');
        // Actualizar el valor del campo en Formik
        props.onChangeText && props.onChangeText(formattedText);
      };
  
    return (
    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
      <Text style={{ fontSize: 16, color: '#888', marginRight: 5 }}>$</Text>
      <TextInput
        {...props}
        onChangeText={handleTextChange}
        keyboardType="numeric"
        style={{ borderWidth: 1, borderColor: Colors.colorCafeRojizo, borderRadius: 15, padding: 10 }}
      />
    </View>
  );
}
