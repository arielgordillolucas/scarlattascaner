import React from 'react';
import {StyleSheet, Platform, StatusBar} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import Colors from '../constants/Colors';

export function showSuccessMsg(message) {
  showMessage({
    description: message,
    duration: 6000,
    message: 'Aviso',
    backgroundColor: Colors.colorVerde,
    titleStyle: style.title,
    textStyle: style.text
  });
}
export function showDangerMsg(message) {
  showMessage({
    description: message,
    duration: 6000,
    message: 'Aviso',
    backgroundColor: Colors.colorRojo,
    titleStyle: style.title,
    textStyle: style.text
  });
}

const style = StyleSheet.create({
  text: {
    fontFamily: Platform.OS === 'ios' ? 'Roboto-Regular' : 'RobotoRegular',
    fontSize: 14,
    color: Colors.colorBlanco,
    fontWeight: 'bold',
    //marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,
    textAlign: 'center'
  },
  title: {
    fontFamily: Platform.OS === 'ios' ? 'Roboto-Regular' : 'RobotoRegular',
    fontSize: 12,
    color: Colors.colorBlanco,
  }
});


