import React from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../selection.json';
const IconFontCustom = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon');

const CustomIcon = (props) => {
  return (
    <IconFontCustom
      allowFontScaling={true}
      name={props.name}
      size={props.size}
      color={props.color}
      style={props.styles}
    />
  );
}

export default CustomIcon;
