import React from 'react';
import { StyleSheet, Platform, TextInput } from 'react-native';
//CONSTANT
import Colors from '../constants/Colors';

export default function CustomInput(props) {
  return (
    <TextInput
      style={props.style ? props.style : styles.inputStyles}
      {...props} //get ahoter props
      value={props.value}
      onChangeText={props.onChangeText}
      placeholder={props.placeholder}
      placeholderTextColor={props.placeholderTextColor}
      maxLength={props.maxLength}
      mode={props.mode}
      editable={!props.disabled}
      keyboardType={props.keyboardType || 'default'}
      defaultValue={props.disabled?"N/A":""}
      underlineColor={props.underlineColor ?? Colors.colorNegro}
      underlineColorAndroid={props.underlineColorAndroid ?? Colors.colorNegro}
      theme={{
        colors: {
          primary: Colors.colorNegro,
          background: 'white',
          text: Colors.colorNegro,
          placeholder: Colors.colorNegro,
          underlineColor: Colors.colorNegro,
        },
        fonts: {
          normal: { fontFamily: Platform.OS === 'ios' ? 'Roboto-Light' : 'RobotoLight' },
          thin: { fontFamily: Platform.OS === 'ios' ? 'Roboto-Light' : 'RobotoLight' },
          regular: { fontFamily: Platform.OS === 'ios' ? 'Roboto-Medium' : 'RobotoMedium' },
        },
      }}
      fontFamily={Platform.OS === 'ios' ? 'Roboto-Light' : 'RobotoLight'}
    />
  );
}

const styles = StyleSheet.create({
  inputStyles: {
    backgroundColor: '#fff',
  },
});
