import React,{Fragment} from 'react';
import { View, ActivityIndicator,StyleSheet } from 'react-native';
import Colors from "../constants/Colors";

export default function Loader(props) {
  return (
    <View
      style={[!props.style ? style.containerLoader : props.style, props.backgroundColor && {backgroundColor: props.backgroundColor}]}
    >
      <ActivityIndicator visible={false} size={props.size} color={props.color ? props.color : Colors.colorNegro} />
    </View>
  );
}

const style = StyleSheet.create({
  /*LOADER*/
  statusLoader: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: Colors.colorBlanco
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  //LOADER
  containerLoader:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.colorBlanco,
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 9999,
  },
  /*NO FOUND DATA*/
  noDataText: {
    color: Colors.colorVerde,
    fontSize:14
  },
});
