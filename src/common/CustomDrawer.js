import React, { useContext } from 'react';
import { Dimensions, SafeAreaView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import LinearGradient from 'react-native-linear-gradient';
//CONSTANTS
import Colors from '../constants/Colors';
//COMMONS
import CustomText from './CustomText';
//SVG
import Sesion from '../assets/svg/salir.svg';
import Avatar from '../assets/svg/avatar.svg';
import { AuthContext } from '../constants/AuthContext';

const screen = Dimensions.get('screen');
export default function CustomDrawer(props) {
  const { authContext, state } = useContext(AuthContext);
  const RenderBackground = ({ children }) => {
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
        locations={[0.7, 0.8, 1]}
        colors={[Colors.colorBlanco, Colors.colorGradiente2, Colors.colorGradiente3]}
        style={styles.linearGradient}
      >
        {children}
      </LinearGradient>
    );
  };

  return (
    <RenderBackground>
      <SafeAreaView style={styles.container}>
        <DrawerContentScrollView
          style={styles.scrollView}
          {...props}
          contentContainerStyle={{ justifyContent: 'space-between', flexGrow: 1 }}
        >
          <View style={styles.menuHeader}>
            <CustomText others={styles.menuTitle}>Menú</CustomText>
            <View style={styles.avatar}>
              <View style={styles.avatarIcon}>
                <Avatar width={screen.width >= 320 ? 50 : 40} height={screen.height >= 480 ? 50 : 40}
                        fill={Colors.colorNegro} />
              </View>
              <View>
                <CustomText>{state.user !== null ? state.user.name : ''}</CustomText>
                <CustomText>{state.user !== null ? state.user.email : ''}</CustomText>
              </View>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <DrawerItemList {...props} />
          </View>
        </DrawerContentScrollView>
        <View style={{ padding: 20 }}>
          <TouchableOpacity
            onPress={() => {
              authContext.signOut();
            }}
            style={{ paddingVertical: 15 }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Sesion width={screen.width >= 320 ? 30 : 25} height={screen.height >= 480 ? 30 : 25}
                      fill={Colors.colorNegro} />
              <CustomText
                others={{
                  fontSize: 15,
                  fontFamily: 'Roboto-Medium',
                  marginLeft: 5,
                }}>
                Cerrar sesión
              </CustomText>
            </View>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </RenderBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    paddingHorizontal: 10,
  },
  menuHeader: {
    paddingVertical: 30,
  },
  menuTitle: {
    fontSize: 30,
    fontWeight: 'bold',
    color: Colors.colorPlomo,
  },
  avatar: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    paddingTop: 20,
  },
  avatarIcon: {},
  avatartext: {},
  linearGradient: {
    flex: 1,
  },
});
