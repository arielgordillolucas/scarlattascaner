import React from 'react';
/*ASYNC*/
import AsyncStorage from '@react-native-async-storage/async-storage';

export const getDataAsync = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (isJsonObject(value)) {
      return value != null ? JSON.parse(value) : null;
    }
    return value;
  } catch (e) {
    console.log(e.message);
  }
};

export const storeDataAsync = async (key, params) => {
  try {
    if (isObject(params)) {
      const jsonValue = JSON.stringify(params);
      await AsyncStorage.setItem(key, jsonValue);
    } else {
      await AsyncStorage.setItem(key, params);
    }
  } catch (e) {
    console.log(e.message);
  }
};

export const removeDataAsync = async (key) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch(e) {
    console.log(e.message);
  }
};

function isJsonObject(data) {
  try {
    JSON.parse(data);
  } catch (e) {
    return false;
  }
  return true;
}

function isObject(data) {
  return typeof data === 'object' && data !== null && !Array.isArray(data);
}
