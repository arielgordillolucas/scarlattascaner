
import axios from 'axios';
import { enablePromise, openDatabase } from 'react-native-sqlite-storage';

const baseURL = 'https://merizalde.scarlattasystem.com/modulos/appMonitoreo/apis/';
// const baseURL = 'http://192.168.100.11/merizalde/modulos/appMonitoreo/apis/';


enablePromise(true); 

export const getDBConnection = async () => {
  const db = openDatabase({ name: 'monitoreo.db', location: 'default' });
  return db;
};
//  users
export async function deleteTableUsers(db) {
  const query = `drop table IF EXISTS users`;
  await db.executeSql(query);
};
export const createTableUsers = async (db) => {
  const query = `CREATE TABLE IF NOT EXISTS users(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_user INTEGER,
        username VARCHAR(512),
        password VARCHAR(512)
    );`;
  return db.executeSql(query);
};
export const insertUsers = async (db) => { 
  const response = await axios.get(baseURL + 'usuarios.php');
  if (response.status === 200) {
    const bloques = response.data.data;
    bloques.forEach(function (resultSet) {
      const query = `INSERT INTO users(id_user,username, password)
      VALUES (
        '${resultSet.Cod_usuario}',
        '${resultSet.Usuario}',
        '${resultSet.Password}'
      );`;
      return db.executeSql(query);
    });
  }
  console.log('Transaccion de users exitosa');
};
export async function getUser(db) {
  const users = [];
  const results = await db.executeSql('SELECT * FROM users'); 
  results.forEach(function (resultSet) {
    for (let index = 0; index < resultSet.rows.length; index++) { 
      users.push(resultSet.rows.item(index)); 
    }
  });  
  return users;
}
// bloques
  export const createTableBloques = async (db) => {
    const query = `CREATE TABLE IF NOT EXISTS bloques(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_table INTEGER,
          bloque INTEGER ,
          cama INTEGER,
          cuadro INTEGER,
          finca INTEGER,
          num_trampa INTEGER,
          trampa_inicial INTEGER
      );`;
    return db.executeSql(query);
  };
  export const insertBloques = async (db) => {

    const response = await axios.get(baseURL + 'bloques.php');
    if (response.status === 200) {
      const bloques = response.data.data;
      bloques.forEach(function (resultSet) {
        const query = `INSERT INTO bloques (id_table, bloque, cama,cuadro,finca,num_trampa,trampa_inicial)
        VALUES (
          '${resultSet.Id}',
          '${resultSet.Bloque}',
          '${resultSet.Camas}',
          '${resultSet.Cuadros}',
          '${resultSet.Finca}',
          '${resultSet.NroTrampas}',
          '${resultSet.TrampaInicial}'
        );`;
        return db.executeSql(query);
      });
    }
    console.log('Transaccion de bloques exitosa');
  };
  export async function getBloques(db) {
    const bloques = [];
    const results = await db.executeSql('SELECT * FROM bloques');
    results.forEach(function (resultSet) {
      for (let index = 0; index < resultSet.rows.length; index++) {
        bloques.push(resultSet.rows.item(index));
      }
    });

    return bloques;
  }
  export async function deleteTableBloques(db) {
    const query = `drop table IF EXISTS bloques`;
    await db.executeSql(query);
  };

// fin bloques

// variedades
  export const createTableVariedad = async (db) => {
    const query = `CREATE TABLE IF NOT EXISTS variedads(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_table INTEGER,
          variedad VARCHAR(512) ,
          tipo INTEGER,
          color INTEGER,
          breeder INTEGER,
          estado INTEGER 
        );`;
    console.log("table creada");

    return db.executeSql(query);
  };
    export const insertVariedades = async (db) => {
      const response = await axios.get(baseURL + 'variedades.php');
      if (response.status === 200) {
        const variedades = response.data.data;
        variedades.forEach(function (resultSet) {
          const query = `INSERT INTO variedads (id_table, variedad, tipo,color,breeder,estado)
          VALUES (
            '${resultSet.Id}',
            '${resultSet.Variedad}',
            '${resultSet.Tipo}',
            '${resultSet.Color}',
            '${resultSet.Breeder}',
            '${resultSet.estado}'
          );`;
          return db.executeSql(query);
        });
      }
      console.log('Transaccion de variedades exitosa');
    };
  export async function getVariedad(db) {
    const variedad = [];
    const results = await db.executeSql('SELECT * FROM variedads');
    results.forEach(function (resultSet) {
      for (let index = 0; index < resultSet.rows.length; index++) {
        variedad.push(resultSet.rows.item(index));
      }
    });
    console.log("variedad", variedad);
    return variedad;
  }
  export async function deleteTableVariedad(db) {
    const query = `drop table IF EXISTS variedads`;
    await db.executeSql(query);
  };
// fin variedades

// plano de monitoreo
 export const createTablePlano = async (db) => {
    const query = `CREATE TABLE IF NOT EXISTS plano_monitoreo(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_table INTEGER,
          bloque INTEGER,
          cama INTEGER,
          variedad INTEGER 
        );`;
    console.log("table lano creada");

    return db.executeSql(query);
  };
 export const insertPlano = async (db) => {
      const response = await axios.get(baseURL + 'plano.php');
      if (response.status === 200) {
        const variedades = response.data.data;
        variedades.forEach(function (resultSet) {
          const query = `INSERT INTO plano_monitoreo (id_table, bloque, cama,variedad)
          VALUES (
            '${resultSet.Id}',
            '${resultSet.Bloque}',
            '${resultSet.Cama}',
            '${resultSet.Variedad}'
          );`;
          return db.executeSql(query);
        });
      }
      console.log('Transaccion de plano exitosa');
  };
export async function getPlano(db, bloques, camas) {

    const plano = []; 
    const results = await db.executeSql(
      'SELECT * FROM plano_monitoreo JOIN variedads ON plano_monitoreo.variedad = variedads.id WHERE plano_monitoreo.bloque = ? AND plano_monitoreo.cama = ?',
      [bloques, camas],
    ); 
    results.forEach(function (resultSet) {
      for (let index = 0; index < resultSet.rows.length; index++) {
        plano.push(resultSet.rows.item(index));
      }
    }); 
    
    return plano;
  }
export async function deleteTablePlano(db) {
    const query = `drop table IF EXISTS plano_monitoreo`;
    await db.executeSql(query);
  };
// fin de plano de monitoreo

// inicio afecciones
 export const createTableAfeccion = async (db) => {
    const query = `CREATE TABLE IF NOT EXISTS causas(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_table_afec VARCHAR(512),
          nombre VARCHAR(512),
          codigo VARCHAR(512),
          color VARCHAR(512), 
          tipo VARCHAR(512), 
          nivel VARCHAR(512) 
        );`;
    console.log("table causas creada");

    return db.executeSql(query);
  };
  export const insertAfeccion = async (db) => {
    const response = await axios.get(baseURL + 'afeccion.php');
    if (response.status === 200) {
      const afeccion = response.data.data;
      afeccion.forEach(function (resultSet) {
        const query = `INSERT INTO causas (id_table_afec, nombre, codigo,color, tipo, nivel)
        VALUES (
          '${resultSet.Id}',
          '${resultSet.Nombre}',
          '${resultSet.Codigo}',
          '${resultSet.Color}',
          '${resultSet.Tipo}',
          '${resultSet.Nivel}'
        );`;
        return db.executeSql(query);
      });
    }
    console.log('Transaccion de afeccion exitosa');
};
export async function getAfeccion(db,tipo) {

    const afeccion = []; 
    const results = await db.executeSql('SELECT * FROM causas WHERE tipo = ?', [tipo]);

    results.forEach(function (resultSet) {
      for (let index = 0; index < resultSet.rows.length; index++) {
        afeccion.push(resultSet.rows.item(index));
      }
    }); 
    return afeccion;
  }
export async function deleteTableAfeccion(db) {
    const query = `drop table IF EXISTS causas`;
    await db.executeSql(query);
  };
// finc afecciones

// inicio de monitoreo directo
export const createTableMonitoreDirecto = async (db) => { 
  const query = `CREATE TABLE IF NOT EXISTS monitoreo_directo(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        fecha DATE,
        hora TIME,
        semana INTEGER,
        bloque INTEGER, 
        nave INTEGER, 
        cama INTEGER, 
        cuadro INTEGER, 
        variedad INTEGER, 
        enfpla INTEGER, 
        tercio INTEGER, 
        nivel INTEGER, 
        estado INTEGER 
      );`;
  console.log("table manitoreo directo creada");

  return db.executeSql(query);
};
export const insertMonitoreDirecto = async (db, data) => {
    data.forEach(function (resultSet) {
      const query = `INSERT INTO monitoreo_directo(fecha, hora, semana,bloque, nave, cama, cuadro, variedad, enfpla, tercio, nivel, estado)
      VALUES (
        '${resultSet.fecha}',
        '${resultSet.hora}',
        '${resultSet.semana}',
        '${resultSet.bloque}',
        '${resultSet.nave}',
        '${resultSet.cama}',
        '${resultSet.cuadro}',
        '${resultSet.variedad}',
        '${resultSet.enfpla}',
        '${resultSet.tercio}',
        '${resultSet.nivel}',
        '${resultSet.estado}'
      );`;
      return db.executeSql(query);
    }); 
  console.log('Transaccion de afeccion exitosa');
  return 200;
};
export async function getMonitoreoDirecto(db) {

  const monitoreo = [];  
  // const results = await db.executeSql('SELECT * FROM monitoreo_directo WHERE estado = 0');
    const results = await db.executeSql(' SELECT m.*, v.variedad, c.nombre AS enfermedad FROM monitoreo_directo m JOIN causas c ON m.enfpla = c.id   JOIN   variedads v ON m.variedad = v.id;');

  results.forEach(function (resultSet) {
    for (let index = 0; index < resultSet.rows.length; index++) {
      monitoreo.push(resultSet.rows.item(index));
    }
  });  
  // console.log(monitoreo, "hola");
  return monitoreo;
}
export async function getMonitoreoIndirecto(db) {

  const monitoreo = [];  
  // const results = await db.executeSql('SELECT * FROM monitoreo_directo WHERE estado = 0');
  const resultsindirec = await db.executeSql('SELECT * FROM monitoreo_indirecto WHERE estado = 0'); 
  resultsindirec.forEach(function (resultsindirec) {
    for (let index = 0; index < resultsindirec.rows.length; index++) { 
      monitoreo.push(resultsindirec.rows.item(index)); 
    }
  }); 
  return monitoreo;
}

export async function getMonitoreoTempe(db) {

  const monitoreo = [];  
  // const results = await db.executeSql('SELECT * FROM monitoreo_directo WHERE estado = 0');
  const resultsindirec = await db.executeSql('SELECT * FROM monitoreo_temperatura WHERE estado = 0'); 
  resultsindirec.forEach(function (resultsindirec) {
    for (let index = 0; index < resultsindirec.rows.length; index++) { 
      monitoreo.push(resultsindirec.rows.item(index)); 
    }
  }); 
  return monitoreo;
}
// fin de monitoreo directo


// incio de tramoas externas
export const createTableTrampasExternas = async (db) => {
  const query = `CREATE TABLE IF NOT EXISTS trampas_externas(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_table_tx INTEGER,
        finca VARCHAR(512),
        trampa INTEGER,
        status INTEGER 
      );`;
  console.log("table trampa externa creada");
  return db.executeSql(query);
};
export const inserTrampasExternas = async (db) => {
    const response = await axios.get(baseURL + 'trampas_externas.php');
    if (response.status === 200) {
      const variedades = response.data.data;
      variedades.forEach(function (resultSet) {
        const query = `INSERT INTO trampas_externas (id_table_tx, finca, trampa,status)
        VALUES (
          '${resultSet.Id}',
          '${resultSet.Finca}',
          '${resultSet.Trampa}',
          '${resultSet.Status}'
        );`;
        return db.executeSql(query);
      });
    }
    console.log('Transaccion de TrampasExternas exitosa');
};
export async function getTrampasExternas(db) { 
  const trampasex = []; 
  const results = await db.executeSql(
    'SELECT * FROM trampas_externas'
  ); 
  results.forEach(function (resultSet) {
    for (let index = 0; index < resultSet.rows.length; index++) {
      trampasex.push(resultSet.rows.item(index));
    }
  }); 
  return trampasex;
}
export async function deleteTrampasExternas(db) {
  const query = `drop table IF EXISTS trampas_externas`;
  await db.executeSql(query);
};
// fin de trampas extrernas

export const createTableTrampasExternasPostc = async (db) => {
  const query = `CREATE TABLE IF NOT EXISTS trampas_externas_pst(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_table_tx INTEGER,
        finca VARCHAR(512),
        trampa INTEGER,
        status INTEGER 
      );`;
  console.log("table trampa externa post creada");
  return db.executeSql(query);
};
export const inserTrampasExternasPostc = async (db) => {
  const response = await axios.get(baseURL + 'trampas_postc.php');
  if (response.status === 200) {
    const variedades = response.data.data;
    variedades.forEach(function (resultSet) {
      const query = `INSERT INTO trampas_externas_pst (id_table_tx, finca, trampa,status)
      VALUES (
        '${resultSet.Id}',
        '${resultSet.Finca}',
        '${resultSet.Trampa}',
        '${resultSet.Status}'
      );`;
      return db.executeSql(query);
    });
  }
  console.log('Transaccion de TrampasExternas exitosa');
};
export async function getTrampasExternasPost(db) { 
  const trampasex = []; 
  const results = await db.executeSql(
    'SELECT * FROM trampas_externas_pst'
  ); 
  results.forEach(function (resultSet) {
    for (let index = 0; index < resultSet.rows.length; index++) {
      trampasex.push(resultSet.rows.item(index));
    }
  }); 
  return trampasex;
}
export async function deleteTrampasExternasPost(db) {
  const query = `drop table IF EXISTS trampas_externas_pst`;
  await db.executeSql(query);
};
// inicio de monitoreo indirecto
export const createTableMonitoreIndirecto = async (db) => { 
  const query = `CREATE TABLE IF NOT EXISTS monitoreo_indirecto(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        responsable INTEGER, 
        fecha DATE,
        hora TIME,
        semana INTEGER,
        bloque INTEGER, 
        finca INTEGER, 
        identificacion INTEGER, 
        tipo INTEGER, 
        trips INTEGER, 
        observacion INTEGER,  
        estado INTEGER 
      );`;
  console.log("table manitoreo indirecto creada");

  return db.executeSql(query);
};
export const insertMonitoreIndirecto = async (db, data) => {
  data.forEach(function (resultSet) {
    const query = `INSERT INTO monitoreo_indirecto(responsable, fecha, hora,semana, bloque, finca, identificacion, tipo, trips, observacion, estado)
    VALUES (
      '${resultSet.responsable}',
      '${resultSet.fecha}',
      '${resultSet.hora}',
      '${resultSet.semana}',
      '${resultSet.bloque}',
      '${resultSet.finca}',
      '${resultSet.identificacion}',
      '${resultSet.tipo}',
      '${resultSet.trips}',
      '${resultSet.observacion}', 
      '${resultSet.estado}'
    );`;
    return db.executeSql(query);
  }); 
console.log('Transaccion de monitoreo indirecto exitosa');
return 200;
};

// finde moitoreo indirecto

// inico de termoometro
export const createTableTermometros = async (db) => {
  const query = `CREATE TABLE IF NOT EXISTS termometros(
        id INTEGER PRIMARY KEY AUTOINCREMENT, 
        id_table_ter INTEGER,
        nombre VARCHAR(512), 
        estado INTEGER 
      );`;
  console.log("table TERMOMETROS  creada");
  return db.executeSql(query);
};

export const inserTermometros= async (db) => {
  const response = await axios.get(baseURL + 'termometros.php');
  if (response.status === 200) {
    const variedades = response.data.data;
    variedades.forEach(function (resultSet) {
      const query = `INSERT INTO termometros (id_table_ter, nombre, estado)
      VALUES (
        '${resultSet.id}',
        '${resultSet.nombre}',
        '${resultSet.estado}' 
      );`;
      return db.executeSql(query);
    });
  }
  console.log('Transaccion de termometros  exitosa');
};
export async function deleteTermometros(db) {
  const query = `drop table IF EXISTS termometros`;
  await db.executeSql(query);
};

// fin de termometros

// Inicop plano de temperatura
export const createTablePlanoTemperatura = async (db) => {
  const query = `CREATE TABLE IF NOT EXISTS plano_temperatura(
        id INTEGER PRIMARY KEY AUTOINCREMENT, 
        id_table_plano INTEGER,
        bloque INTEGER, 
        termometro INTEGER 
      );`;
  console.log("table plano temperatura  creada");
  return db.executeSql(query);
};
export const insertPlanoTemperatura= async (db) => {
  const response = await axios.get(baseURL + 'plano_temperatura.php');
  if (response.status === 200) {
    const variedades = response.data.data;
    variedades.forEach(function (resultSet) {
      const query = `INSERT INTO plano_temperatura (id_table_plano, bloque, termometro)
      VALUES (
        '${resultSet.id}',
        '${resultSet.bloque}',
        '${resultSet.termometro}' 
      );`;
      return db.executeSql(query);
    });
  }
  console.log('Transaccion de plano temperatura  exitosa');
};
export async function deleteTemperatura(db) {
  const query = `drop table IF EXISTS plano_temperatura`;
  await db.executeSql(query);
};
export async function getPlanoTemperatura(db, bloque) { 
    const user = []
    const results = await db.executeSql('SELECT bloques.bloque, plano_temperatura.*, termometros.* FROM bloques     JOIN plano_temperatura ON bloques.bloque = plano_temperatura.bloque     JOIN termometros ON plano_temperatura.termometro = termometros.id   WHERE bloques.bloque = ?', [parseInt(bloque,10)]);
    results.forEach(function (resultSet) {
      for (let index = 0; index < resultSet.rows.length; index++) {
        user.push(resultSet.rows.item(index));

      }
    });
    console.log(user);
    return user;
   
}
// fin plano de temperatura

// Monitore temperatura
export const createTableMonitoreoTemperatura = async (db) => { 
  const query = `CREATE TABLE IF NOT EXISTS monitoreo_temperatura(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        fecha DATE,
        hora TIME,
        semana INTEGER,
        bloque INTEGER, 
        termometro INTEGER,
        temperatura VARCHAR(512),
        humedad VARCHAR(512),
        estado INTEGER 
      );`;
  console.log("table manitoreo temper creada");

  return db.executeSql(query);
};
export const insertMonitoreoTemperatura = async (db, data) => {
  data.forEach(function (resultSet) {
    const query = `INSERT INTO monitoreo_temperatura(fecha, hora, semana, bloque, termometro,temperatura,humedad, estado)
    VALUES (
      '${resultSet.fecha}',
      '${resultSet.hora}',
      '${resultSet.semana}',
      '${resultSet.bloque}',
      '${resultSet.termometro}',
      '${resultSet.temperatura}',
      '${resultSet.humedad}', 
      '${resultSet.estado}'
    );`;
    return db.executeSql(query);
  }); 
console.log('Transaccion de tempertaura exitosa');
return 200;
};
export async function deleteTem(db) {
  const query = `drop table IF EXISTS monitoreo_temperatura`;
  await db.executeSql(query);
  console.log("borrado");
};
export async function deletedire(db) {
  const query = `drop table IF EXISTS monitoreo_directo`;
  await db.executeSql(query);
};
export async function deleteIndi(db) {
  const query = `drop table IF EXISTS monitoreo_indirecto`;
  await db.executeSql(query);
};
export const post = async () => {
  const monito_directo = [];
  const monito_inddirecto = [];
  const monito_remperatr = [];
  const db = await getDBConnection();
  await createTableMonitoreDirecto(db);
  await createTableMonitoreIndirecto(db); 
  await createTableMonitoreoTemperatura(db); 

  const results = await db.executeSql('SELECT * FROM monitoreo_directo WHERE estado = 0'); 
  results.forEach(function (resultSet) {
    for (let index = 0; index < resultSet.rows.length; index++) { 
      monito_directo.push(resultSet.rows.item(index)); 
    }
  });

  const resultstemp = await db.executeSql('SELECT * FROM monitoreo_temperatura WHERE estado = 0'); 
  resultstemp.forEach(function (resultstemp) {
    for (let index = 0; index < resultstemp.rows.length; index++) { 
      monito_remperatr.push(resultstemp.rows.item(index)); 
    }
  });

  const resultsindirec = await db.executeSql('SELECT * FROM monitoreo_indirecto WHERE estado = 0'); 
  resultsindirec.forEach(function (resultsindirec) {
    for (let index = 0; index < resultsindirec.rows.length; index++) { 
      monito_inddirecto.push(resultsindirec.rows.item(index)); 
    }
  });
  postData = {
    "directo": monito_directo,
    "indirecto": monito_inddirecto,
    "temperatura": monito_remperatr
    };

  const response = await axios.post(baseURL + 'upmonitoreos.php', postData, {
    headers: {
      'Content-Type': 'application/json',
    
    },
  });

  if(response.status === 200){
    const db = await getDBConnection();
    await deleteTem(db);
    await deletedire(db);
    await deleteIndi(db); 
    db.close(); 
  
    return 200;
  }else{
    db.close(); 
  return 400;
  }


};


// post


export async function initDatabase() {
  const db = await getDBConnection(); 
  // await deleteTableUsers(db);
  await createTableUsers(db);
  await insertUsers(db);
  db.close();

};




export async function initDatabaseBloques() {
  const db = await getDBConnection();
  // bloques
  await deleteTableBloques(db);
  await createTableBloques(db);
  await insertBloques(db);
  // await getBloques(db);
  //  variedades
  await deleteTableVariedad(db);
  await createTableVariedad(db);
  await insertVariedades(db);
  // await getVariedad(db);
  // plano
  await deleteTablePlano(db);
  await createTablePlano(db);
  await insertPlano(db);
  // await getPlano(db); 
  // causas
  await deleteTableAfeccion(db);
  await createTableAfeccion(db);
  await insertAfeccion(db);
  // monitore directo 
  await createTableMonitoreDirecto(db);

  // trampas externas
  await deleteTrampasExternas(db);
  await createTableTrampasExternas(db);
  await inserTrampasExternas(db);
  //  await getTrampasExternas(db);

  await deleteTrampasExternasPost(db);
  await createTableTrampasExternasPostc(db);
  await inserTrampasExternasPostc(db);
  
  // externas postc

  await deleteTermometros(db);
  await createTableTermometros(db);
  await inserTermometros(db);

  // plano temperatura
  await deleteTemperatura(db);
  await createTablePlanoTemperatura(db);
  await insertPlanoTemperatura(db);


  // monitoreo indirecto
  await createTableMonitoreIndirecto(db); 

  // Monitoreo Temperatura
  await createTableMonitoreoTemperatura(db); 

  db.close(); 
  return 200;
};

export async function initGetsBloques() {
  const db = await getDBConnection();
  await getBloques(db);
  db.close(); 
};
 