import React, { useState, useEffect, useRef, useCallback } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Platform,
    StyleSheet,
    StatusBar,
    ScrollView,
    Alert,
    ActivityIndicator
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Colors from '../../constants/Colors';
import { useTheme } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Image } from 'react-native-elements';
import Users from '../../model/users';
const Login = ({ navigation }) => {
    const [isConnected, setIsConnected] = useState(null);
    const [data, setData] = React.useState({
        username: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
    });
    const [user, setUsers] = useState(null);
    const [loading, setLoading] = useState(false);
    const handleLoadData = () => {
        setLoading(true);

    };


    useEffect(() => {
        const checkInternetConnection = async () => {
            try {
                const response = await fetch('https://www.google.com', { method: 'HEAD' });
                const isConnected = response.status === 200;
                setIsConnected(isConnected);
                db.close();
            } catch (error) {
                setIsConnected(false);
            }
        };

        checkInternetConnection();
    }, []);
    const textInputChange = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                username: val,
                check_textInputChange: true,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                username: val,
                check_textInputChange: false,
                isValidUser: false
            });
        }
    }

    const handlePasswordChange = (val) => {
        if (val.trim().length >= 8) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const handleValidUser = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                isValidUser: false
            });
        }
    }

    const loginHandle = async (userName, password) => {
        handleLoadData();
        let foundUser = [];
        foundUser = Users.filter(item => {
            return userName == item.username && password == item.password;
        });
        if (data.username.length == 0 || data.password.length == 0) {
            Alert.alert('¡Error!', 'Usuario y contraseña no pueden estar vacios.', [
                { text: 'Okay' }
            ]);
            setLoading(false);
            return;
        }

        if (foundUser.length == 0) {
            Alert.alert('¡Usuario invalido!', 'El usuario o la contraseña son incorrectos.', [
                { text: 'Okay' }
            ]);
            setLoading(false);

            return;
        }

        // AsyncStorage.setItem('token',foundUser);
        AsyncStorage.setItem('token', JSON.stringify(foundUser));
        Alert.alert('¡Bienvenido!', 'Inicio de sesión exitoso.', [
            { text: 'Okay' }
        ]);
        setLoading(false);

    }

    return (

        <View style={styles.container}>
            {loading ? (
            <ActivityIndicator size="large" style={{ paddingTop: 150 }} />) :
                <><StatusBar />
                 <ScrollView  >
                <View >
                    {/* <View style={styles.logo}>
                        <Image
                            source={require('../../assets/scarlatta.png')}
                            style={{ width: 200, height: 200 }} /> 
                    </View> */}

                    <View
                        style={{
                            backgroundColor: '#CC3399', padding: 30,
                            borderBottomLeftRadius: 60
                        }}>

                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                            <Image
                                style={{
                                    width: 180, height: 180,
                                    resizeMode: 'contain'
                                }}
                                source={require('../../assets/logo.png')} />
                        </View>

                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            <Text style={{
                                fontWeight: '500', fontSize: 20,
                                color: '#fff'
                            }}>Transporte</Text>
                            <Text style={{
                                fontWeight: '300', fontSize: 15,
                                color: '#fff'
                            }}>Recorridos</Text>
                        </View>
                    </View>
                </View>
                    <Animatable.View
                        animation="fadeInUpBig"
                        style={[styles.footer, {
                            backgroundColor: 'white',
                        }]}
                    >
                        <Text style={[styles.text_footer, {

                        }]}>Usuario</Text>
                        <View style={styles.action}>
                            <FontAwesome
                                name="user-o"
                                color={Colors.rojo1}
                                size={20} />
                            <TextInput
                                placeholder="Usuario"
                                placeholderTextColor="#666666"
                                style={[styles.textInput, {
                                    color: Colors.rojo1,
                                }]}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                                onEndEditing={(e) => handleValidUser(e.nativeEvent.text)} />
                            {data.check_textInputChange ?
                                <Animatable.View
                                    animation="bounceIn"
                                >
                                    <Feather
                                        name="check-circle"
                                        color="green"
                                        size={20} />
                                </Animatable.View>
                                : null}
                        </View>
                        {data.isValidUser ? null :
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>Este campo debe tener mas de 4 números</Text>
                            </Animatable.View>}


                        <Text style={[styles.text_footer, {
                            color: Colors.rojo1,
                            marginTop: 35
                        }]}>Contraseña</Text>
                        <View style={styles.action}>
                            <Feather
                                name="lock"
                                color={Colors.rojo1}
                                size={20} />
                            <TextInput
                                placeholder="Contraseña"
                                placeholderTextColor="#666666"
                                secureTextEntry={data.secureTextEntry ? true : false}
                                style={[styles.textInput, {
                                    color: Colors.rojo1,
                                }]}
                                autoCapitalize="none"
                                onChangeText={(val) => handlePasswordChange(val)} />
                            <TouchableOpacity
                                onPress={updateSecureTextEntry}
                            >
                                {data.secureTextEntry ?
                                    <Feather
                                        name="eye-off"
                                        color="grey"
                                        size={20} />
                                    :
                                    <Feather
                                        name="eye"
                                        color="grey"
                                        size={20} />}
                            </TouchableOpacity>
                        </View>
                        {data.isValidPassword ? null :
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>La contraseña debe tener mas de 8 digitos.</Text>
                            </Animatable.View>}

                        <View style={styles.button}>
                            <TouchableOpacity
                                style={styles.signIn}
                                onPress={() => { loginHandle(data.username, data.password); }}
                            >
                                <LinearGradient
                                    colors={['#CC3399', '#CC3399']}
                                    style={styles.signIn}
                                >
                                    <Text style={[styles.textSign, {
                                        color: '#fff'
                                    }]}>Iniciar Sesión</Text>
                                </LinearGradient>
                            </TouchableOpacity>



                        </View>
                    </Animatable.View>
                    </ScrollView>
                    </>
            }
        </View>
    );
};

export default Login;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    logo: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 15
    },

    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 1.8,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#fff',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        fontSize: 14,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});
