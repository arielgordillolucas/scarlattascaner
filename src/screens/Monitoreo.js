import React, { useState, useEffect, useRef, useCallback } from 'react';
import { StyleSheet, View, Text, useWindowDimensions, Alert } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Colors from '../constants/Colors';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';
import CustomText from '../common/CustomText';
import { Field, Formik } from 'formik';
import { TouchableOpacity } from 'react-native';
import RNPickerSelect from 'react-native-picker-select'; 
import LinearGradient from 'react-native-linear-gradient';
import { TextInput } from 'react-native';
import {getTrampasExternas, createTableMonitoreDirecto, createTableMonitoreIndirecto, getDBConnection, insertMonitoreIndirecto, getBloques, getPlano, getAfeccion, insertMonitoreDirecto, getTrampasExternasPost } from '../ultis/DB';
import { useFocusEffect } from '@react-navigation/native';
const fecha =  new Date().toLocaleDateString('es-EC', { year: 'numeric', month: '2-digit', day: '2-digit', timeZone: 'America/Guayaquil' }).split('/').reverse().join('-'); 
const formattedWeekNumber = `${(new Date()).getFullYear().toString().slice(-2)}${(d => d < 10 ? '0' + d : d)(Math.floor((new Date() - new Date((new Date()).getFullYear(), 0, 1)) / 604800000) + 1)}`; 

const renderTabBar = props => (
  <TabBar
    {...props}
    indicatorStyle={{ backgroundColor: 'white' }}
    style={{ backgroundColor: Colors.colorCafeRojizo }}
  />
);
const MonitoreoDirecto = () => {
  const [datos, setDatos] = useState([]);
  const [bloque, setBloque] = useState(null);
  const [cama, setCama] = useState(null);
  const [cuadro, setCuadro] = useState(null);
  const [variedad, setVariedad] = useState(null);
  const [typeafec, settypeafec] = useState(null);
  const [plaga, setplaga] = useState(null);
  const [enfer, setenfer] = useState(null);
  const [tercio, setercio] = useState(null);
  const [nivel, setnivel] = useState(null);
  const [resps, setResps] = useState(null);
  const items = [];
  const data_camas = [];
  const data_cuadros = [];
  const data_var = [];
  const data_afect = [];
  const [data_bloques, setData_bloques] = useState([]);
  const [data_variedad, setData_variedad] = useState([]);
  const [data_plaaga, setData_plaga] = useState([]);
  const bloqueRef = useRef(null);
  const camaRef = useRef(null);
  const typeefecRef = useRef(null);
  const [valorPorDefecto, setValorPorDefecto] = useState(null);

  const resultado = datos.map((resultSet, index) => {
    const { bloque, cama, hora } = resultSet;
    return `${index + 1}.  Bloque: ${bloque}, Cama: ${cama},Hora: ${hora} `;
  });
  const mostrarDialogo = () => {
    Alert.alert(
      'Guardar',

      resultado.join('\n'),
      [
        {
          text: 'Cancelar',
          style: 'cancel',
        },
        {
          text: 'Guardar',
          onPress: async () => {

            save();
            await resps;

            
              setDatos([]);
              setBloque(null);
              setCama(null);
              setCuadro(null);
              settypeafec(null);
              setplaga(null);
              setenfer(null);
              setercio(null);
              setnivel(null);
              Alert.alert('Monitoreo guardados exitosamente', 'Los monitoreos se han guardado correctamente.');
            

          },
        },
      ],
      { cancelable: false }
    );
  };
  const succes = () => {

    // Muestra el alert después de 5 segundos
    Alert.alert('Monitoreo generado exitosamente', 'No olvides dar Guardar, cuando termines de realizar el monitoreo.');

    // Limpia el temporizador al desmontar el componente (opcional)
    return () => clearTimeout(timer);

  }
  const handleClick = () => {
    // Aquí puedes realizar alguna lógica antes de guardar los datos
    const nuevoDato = {
      responsable: 1,
      fecha: fecha,
      hora: new Date().toLocaleTimeString('es-EC', { timeZone: 'America/Guayaquil' }),
      semana: parseInt(formattedWeekNumber, 10),
      bloque: parseInt(bloque[0], 10),
      nave: 0,
      cama: parseInt(cama.value, 10),
      cuadro: parseInt(cuadro.value, 10),
      variedad: parseInt(variedad, 10),
      enfpla: plaga.value === null ? parseInt(enfer.value, 10) : parseInt(plaga.value, 10),
      tercio: parseInt(tercio.value, 10),
      nivel: parseInt(nivel.value, 10),
      estado: 0
    };
    console.log(nuevoDato);
    // Actualiza el estado del arreglo añadiendo el nuevo dato
    setDatos((prevDatos) => [...prevDatos, nuevoDato]);
    console.log(datos,"datos");
    succes();
   
  };

  const focus = useCallback(function () {
    async function fetchBloques() {
      const db = await getDBConnection();
      const bloqd = await getBloques(db);
      setData_bloques(bloqd);
      db.close();
    }
    fetchBloques();
  }, []);

  useFocusEffect(focus);

  const save = async () => {
    const db = await getDBConnection();
    await createTableMonitoreDirecto(db);
    const bloqd = await insertMonitoreDirecto(db, datos);
    setResps(bloqd);
    db.close();
  }


  // Transforma los datos de data_bloques en el formato esperado por RNPickerSelect
  data_bloques.forEach(function (resultSet) {
    items.push({ label: "Bloque " + resultSet.bloque.toString(), value: [resultSet.bloque.toString(), resultSet.cama.toString(), resultSet.cuadro.toString(), resultSet.num_trampa.toString(), resultSet.trampa_inicial.toString()] , color: Colors.rojo4});
  });
  if (bloque != null) {
    for (let i = 1; i <= parseInt(bloque[1], 10); i++) {
      data_camas.push({ label: "Cama " + `${i}`, value: `${i}` , color: Colors.rojo4});
    }
    for (let j = 1; j <= parseInt(bloque[2], 10); j++) {
      data_cuadros.push({ label: "Cuadro " + `${j}`, value: `${j}`, color: Colors.rojo4 });
    }
  }


  async function fetchVar() {
    if (bloque && cama) {
      const db = await getDBConnection();
      const vari = await getPlano(db, parseInt(bloque[0], 10), cama.value)
      setData_variedad(vari);
      db.close();
    }
  }
  async function waitForAfeccion() {
    const db = await getDBConnection();
    const vari = await getAfeccion(db, typeafec.value === 'Plaga' ? "2" : (typeafec.value === 'Enfermedad' ? "1" : null));
    setData_plaga(vari);
    db.close();
  }
  useEffect(() => {
    // Verifica si hay cambios en bloque o cama
    if (bloque !== bloqueRef.current || cama !== camaRef.current) {
      // Actualiza las referencias con los nuevos valores
      bloqueRef.current = bloque;
      camaRef.current = cama;
      fetchVar();
    }
  }, [bloque, cama]);

  useEffect(() => {
    // Verifica si hay cambios en bloque o cama
    if (typeafec !== typeefecRef.current) {
      typeefecRef.current = typeafec;
      waitForAfeccion();
    }
  }, [typeafec]);

  data_variedad.forEach(function (resultSet) {
    data_var.push({ label: resultSet.variedad.toString(), value: resultSet.id_table.toString(), color: Colors.rojo4 });
  });
 
  data_plaaga.forEach(function (resultSet) {
    data_afect.push({ label: resultSet.nombre.toString(), value: resultSet.id_table_afec.toString(), color: Colors.rojo4 });

  });  
 console.log(data_var);
  return (
    <ScrollView contentContainerStyle={{ flexGrow: 50, justifyContent: 'flex-start' }}>

      <Formik>
        {({ }) => (
          <>
            <View style={stylerows.container}>
              {/* Fila 1 */}
              <View style={stylerows.gridItem}>
                <Text style={styleText.text}> Bloque</Text>

                <RNPickerSelect
                  style={{
                    placeholder: {
                      color: '#340000',
                      fontSize: 12,
                      fontWeight: 'bold',
                    },
                  }}
                  placeholder={{ label: 'Seleccione...', color: '#340000' }}
                  onValueChange={(value) => setBloque(value)}
                  items={items}
                />
              </View>
              <View style={stylerows.gridItem}>

                {bloque != null ? (
                  <><Text style={styleText.text}> Cama</Text><RNPickerSelect
                    style={{
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}

                    onValueChange={value => {
                      setCama({ value });
                    }}
                    items={data_camas}
                  /></>
                ) : (
                  <></>
                )}
              </View>
              <View style={stylerows.gridItem}>
                {cama != null ? (
                  <><Text style={styleText.text}> Cuadro</Text><RNPickerSelect
                    style={{
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}
                    onValueChange={value => {
                      setCuadro({ value });
                    }}
                    items={data_cuadros} /></>
                ) : (
                  <></>
                )}
              </View>

              <View style={stylerows.gridItem}>
                {bloque != null &&
                  cama != null &&
                  cuadro != null ? (
                    data_var.length > 0 ? (
                      <><Text style={styleText.text}> Variedad</Text>
                   <Text style={styleText.text}> {data_var !== null ? data_var[0].label : "N/A"} </Text>
                   { setVariedad(data_var !== null ? data_var[0].value : null)}
                   </>
                    ):(<></>)
                ) : (
                  <></>
                )}
              </View>
              <View style={stylerows.gridItem2}>
                {bloque != null &&
                  cama != null &&
                  cuadro != null &&
                  variedad != null ? (
                  <><Text style={styleText.text}> Tipo de Afección</Text><RNPickerSelect
                    style={{
                      alignItems: 'center',
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}
                    onValueChange={value => {
                      settypeafec({ value });
                    }}
                    items={[
                      { label: 'Plaga', value: 'Plaga' ,color: Colors.rojo4},
                      { label: 'Enfermedad', value: 'Enfermedad' ,color: Colors.rojo4},
                    ]} /></>
                ) : (
                  <></>
                )}
              </View>
              <View style={stylerows.gridItem2}>
                {typeafec != null ? (

                  <><Text style={styleText.text}> {typeafec.value}</Text><RNPickerSelect
                    style={{
                      alignItems: 'center',
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}
                    onValueChange={value => {
                      setplaga({ value });
                    }}
                    items={data_afect} /></>

                ) : (
                  <></>
                )}
              </View>

              <View style={stylerows.gridItem}>
                {( enfer != null || plaga != null) ? (
                  <><Text style={styleText.text}> Tercio</Text><RNPickerSelect
                    style={{
                      alignItems: 'center',
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}
                    onValueChange={value => {
                      setercio({ value });
                    }}
                    items={[
                      { label: 'B - Bajo', value: '1',color: Colors.rojo4 },
                      { label: 'M - Medio', value: '2' ,color: Colors.rojo4},
                      { label: 'A - Alto', value: '3' ,color: Colors.rojo4},
                    ]} /></>
                ) : (
                  <></>
                )}
              </View>
              <View style={stylerows.gridItem}>
                {(tercio != null) ? (
                  <><Text style={styleText.text}> Nivel</Text><RNPickerSelect
                    style={{
                      alignItems: 'center',
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}
                    onValueChange={value => {
                      setnivel({ value });
                    }}
                    items={[
                      { label: 'Nivel 1', value: '1',color: Colors.rojo4 },
                      { label: 'Nivel 2', value: '2', color: Colors.rojo4},
                      { label: 'Nivel 3', value: '3', color: Colors.rojo4},
                    ]} /></>
                ) : (
                  <></>
                )}
              </View>


              <View style={stylerows.gridItem}>
                {(bloque != null && cama != null && cuadro != null && variedad != null && typeafec != null && (plaga != null || enfer != null) && tercio != null && nivel != null)
                  ? (<TouchableOpacity
                    style={{
                      width: '100%',
                      height: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 10
                    }}
                    onPress={handleClick}
                  >
                    <LinearGradient
                      colors={['#a41105', '#DF4D31']}
                      style={{
                        width: '100%',
                        height: 50,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 10
                      }}
                    >
                      <Text style={[styles.textSign, {
                        color: '#fff'
                      }]}>Agregar</Text>
                    </LinearGradient>
                  </TouchableOpacity>) :
                  <></>
                }

              </View>
              <View style={stylerows.gridItem}>
                {(datos.length !== 0) ? (<TouchableOpacity
                  style={{
                    width: '100%',
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10
                  }}
                  onPress={mostrarDialogo}
                >
                  <LinearGradient
                    colors={['#DF4D31', '#a41105']}
                    style={{
                      width: '100%',
                      height: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 10
                    }}
                  >
                    <Text style={[styles.textSign, {
                      color: '#fff'
                    }]}>Guardar</Text>
                  </LinearGradient>
                </TouchableOpacity>) :
                  <></>
                }
              </View>
            </View>


          </>
        )}
      </Formik>
    </ScrollView>
  );
};
const MonitoreoIndirecto = () => {
  const [datost, setDatost] = useState([]);
  const [bloquet, setBloquet] = useState(null);
  const [trampa, settrampa] = useState({});
  const [placa, setplaca] = useState(null);
  const [placaex, setplacaex] = useState(null);
  const [trip, settrip] = useState(null);
  const [observ, setobserv] = useState(null);
  const data_tx = [];
  const data_pst = [];
  const items = [];
  const data_placas_inter = [];
  const [data_trampas_exter, setData_trampas_exter] = useState([]);
  const [data_trampas_exter_pst, setData_trampas_exter_pst] = useState([]);
  const [data_bloques, setData_bloques] = useState([]);
  const [respst, setRespst] = useState(null);


  const resultado = datost.map((resultSet, index) => {
    const { tipo, fecha, hora ,trips } = resultSet;
    return `${index + 1}.  Tipo: ${tipo === 1 ? "Interna" : (tipo === 3 ? 'Postcosecha' : "Externa")}, Hora: ${hora} ,  Tips: ${trips}`;
  });

  const mostrarDialogo = () => {
    Alert.alert(
      'Guardar',

      resultado.join('\n'),
      [
        {
          text: 'Cancelar',
          style: 'cancel',
        },
        {
          text: 'Guardar',
          onPress: async () => {
            save();
            await respst;

              setDatost([]);
              setBloquet(null);
              settrampa({});
              setplaca(null);
              setplacaex(null);
              settrip(null);
              setobserv(null); 
              Alert.alert('Monitoreo guardados exitosamente', 'Los monitoreos se han guardado correctamente.');
           

          },
        },
      ],
      { cancelable: false }
    );
  };
  const succes = () => {

    // Muestra el alert después de 5 segundos
    Alert.alert('Monitoreo generado exitosamente', 'No olvides dar Guardar, cuando termines de realizar el monitoreo.');
 
    // Limpia el temporizador al desmontar el componente (opcional)
    return () => clearTimeout(timer);

  }

  const save = async () => {
    const db = await getDBConnection(); 
    await createTableMonitoreIndirecto(db); 
    const bloqd = await insertMonitoreIndirecto(db, datost);
    setRespst(bloqd);
    db.close();
  }
  const handleClick = () => {
    // Aquí puedes realizar alguna lógica antes de guardar los datos

    const nuevoDato = {
      responsable: 1,
      fecha: fecha,
      hora: new Date().toLocaleTimeString('es-EC', { timeZone: 'America/Guayaquil' }),
      semana: parseInt(formattedWeekNumber, 10),
      bloque: trampa.value === "2" ? 0 : (bloquet !== null ? bloquet.value[0] : 0), 
      finca:  bloquet !== null ? bloquet.value[5] : parseInt(placaex.value[1], 10),
      identificacion: (trampa.value === "1"  ) ? placa.value: ( trampa.value === "2" || trampa.value === "3" ? placaex.value[0] : 0 )  ,
      tipo: trampa.value === "1" ? 1 : (trampa.value === "3" ? 3 : 2),
      trips: parseInt(trip,10) > 3 ?  3 : parseInt(trip,10),
      observacion:  parseInt(trip,10) > 3 ? ("trip sobrepaso los 3: Trips Totales : " + parseInt(trip,10) + " " +  observ ) : observ,
      estado:  0
     
    };
    console.log(nuevoDato); 
    setDatost((prevDatos) => [...prevDatos, nuevoDato]);
    console.log(datost, "todos");
    succes();
   
    settrip(null);
    setobserv(null);

  };
  const focus = useCallback(function () {
    async function fetchTrampasEx() {
      const db = await getDBConnection();
      const bloqd = await getTrampasExternas(db);
      setData_trampas_exter(bloqd);
      db.close();
    }
    fetchTrampasEx();
  }, []);
  useFocusEffect(focus);
  
  const focus2 = useCallback(function () {
    async function fetchBloques() {
      const db = await getDBConnection();
      const bloqd = await getBloques(db);
      setData_bloques(bloqd);
      db.close();
    }
    fetchBloques();
  }, []);
  useFocusEffect(focus2);

  const focus3 = useCallback(function () {
    async function fetchTrampasEx() {
      const db = await getDBConnection();
      const bloqd = await getTrampasExternasPost(db);
      setData_trampas_exter_pst(bloqd);
      db.close();
    }
    fetchTrampasEx();
  }, []);
  useFocusEffect(focus3); 
  data_trampas_exter.forEach(function (resultSet) {
    data_tx.push({ label: resultSet.trampa, value: [resultSet.id_table_tx, resultSet.finca],color: Colors.rojo4 });
  });

  data_trampas_exter_pst.forEach(function (resultSet) {
    data_pst.push({ label: resultSet.trampa, value: [resultSet.id_table_tx, resultSet.finca],color: Colors.rojo4 });
  });

  data_bloques.forEach(function (resultSet) {
    items.push({ label: "Bloque " + resultSet.bloque.toString(), value: [resultSet.bloque.toString(), resultSet.cama.toString(), resultSet.cuadro.toString(), resultSet.num_trampa.toString(), resultSet.trampa_inicial.toString(),resultSet.finca.toString()] , color: Colors.rojo4});
  });
     if (bloquet != null) { 

    for (let i = bloquet.value[4]; i <= (parseInt(bloquet.value[3], 10) + (parseInt(bloquet.value[4], 10) - 1)); i++) {
      data_placas_inter.push({ label: "Placa " + `${i}`, value: `${i}`, color: Colors.rojo4 });
    } 
  }
  const handleTextChange = (text) => {
    // Validar que el valor ingresado sea numérico
    const numericValue = parseFloat(text);

    // Validar si el valor está dentro del rango permitido (-5 a 50)
    if (!isNaN(numericValue) && numericValue <= 50) {
    
      settrip(text);
    }else{
      settrip(null);
      Alert.alert('Alerta', "Solo se acetan digitos numerios y valores desde los -5 °C hasta los 50 °C.")
    } 
    // Puedes agregar un mensaje de error o realizar otras acciones si el valor es inválido
  }; 
  return (
    <ScrollView contentContainerStyle={{ flexGrow: 50, justifyContent: 'flex-start' }}>

      <Formik>
        {({ }) => (
          <>
            <View style={stylerows.container}>
              {/* Fila 1 */}
              <View style={stylerows.gridItem2}>
                <Text style={styleText.text}> Trampa</Text>
                <RNPickerSelect
                  style={{
                    placeholder: {
                      color: '#340000',
                      fontSize: 12,
                      fontWeight: 'bold',
                    },
                  }}
                  placeholder={{ label: 'Seleccione...', color: '#340000' }}

                  onValueChange={value => {
                    settrampa({ value });
                  }}
                  items={[
                    { label: 'Interna', value: '1', color: Colors.rojo4},
                    { label: 'Externa', value: '2', color: Colors.rojo4 },
                    { label: 'Postcosecha', value: '3', color: Colors.rojo4 }
                  ]}
                />
              </View>
              <View style={stylerows.gridItem2}> 
                {trampa.value === "2" || trampa.value === '3'? (
                  <><Text style={styleText.text}> # de Placa</Text><RNPickerSelect
                    style={{
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}
                    onValueChange={value => {
                      setplacaex({ value });
                    }}
                    items={ trampa.value === '2' ? data_tx : (trampa.value === '3' ? data_pst : data_tx)} /></>
                ) : (
                  <></>
                )}
              </View>
              <View style={stylerows.gridItem2}> 
                {trampa.value === "1"  ? (
                  <><Text style={styleText.text}> Bloque</Text><RNPickerSelect
                    style={{
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}
                    onValueChange={value => {
                      setBloquet({ value });
                    }}
                    items={items} /></>
                ) : (
                  <></>
                )}
              </View> 
              <View style={stylerows.gridItem2}> 
                {(trampa.value === '1') && bloquet != null  ? (
                  <><Text style={styleText.text}> # de Placa</Text><RNPickerSelect
                    style={{
                      placeholder: {
                        color: '#340000',
                        fontSize: 12,
                        fontWeight: 'bold',
                      },
                    }}
                    placeholder={{ label: 'Seleccione...', color: '#340000' }}
                    onValueChange={value => {
                      setplaca({ value });
                    }}
                    items={ data_placas_inter} /></>
                ) : (
                  <></>
                )}
              </View>
              <View style={stylerows.gridItem2}>

                {placa != null || bloquet != null ||placaex != null ? (
                  <><Text style={styleText.text}> # de Trip</Text>
                    <TextInput
                      keyboardType="numeric" // Esto establece el teclado en modo numérico
                      placeholder="Ingrese un número"
                      value={trip}
                      onChangeText={handleTextChange}
                    />
                  </>
                ) : (
                  <></>
                )}
              </View>

              <View style={stylerows.gridItem2}>

                {placa != null || bloquet != null ||placaex != null ? (
                  <><Text style={styleText.text}> Observación</Text>
                    <TextInput
                      keyboardType="text" // Esto establece el teclado en modo numérico
                      placeholder="Ingrese las observación del trip"
                      onChangeText={(text) => setobserv(text)}
                    />
                  </>
                ) : (
                  <></>
                )}
              </View>
              <View style={stylerows.gridItem}>

                {(trampa.value != null && (placa != null || placaex != null)  && trip != null) ?
                  (<TouchableOpacity
                  style={{
                    width: '100%',
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10
                  }}
                  onPress={handleClick}
                >
                  <LinearGradient
                    colors={['#a41105', '#DF4D31']}
                    style={{
                      width: '100%',
                      height: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 10
                    }}
                  >
                    <Text style={[styles.textSign, {
                      color: '#fff'
                    }]}>Agregar</Text>
                  </LinearGradient>
                </TouchableOpacity>) :
                <></>
                }
              </View>
              <View style={stylerows.gridItem}>
                { (datost.length !== 0) ?
                (<TouchableOpacity
                  style={{
                    width: '100%',
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10
                  }}
                  onPress={mostrarDialogo}
                >
                  <LinearGradient
                    colors={['#DF4D31', '#a41105']}
                    style={{
                      width: '100%',
                      height: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 10
                    }}
                  >
                    <Text style={[styles.textSign, {
                      color: '#fff'
                    }]}>Guardar</Text>
                  </LinearGradient>
                </TouchableOpacity>) : 
                <></>
                
              }
              </View>
            </View>


          </>
        )}
      </Formik>
    </ScrollView>
  );
};
const FirstRoute = () => (

  <SafeAreaView style={styles.container}>
    <ScrollView
      style={styles.scrollView}
      showsVerticalScrollIndicator={true}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'space-between',
      }}>
      <View style={styles.page}>
        <View style={styles.header}>
          <View style={styles.title}>
            <CustomText others={styles.textTitle}>Monitoreo</CustomText>
            <Text style={{color:'black'}}>Fecha: {fecha}</Text>
            <Text style={{color:'black'}}>Semana: {formattedWeekNumber}</Text>

          </View>
        </View>
        <MonitoreoDirecto />
      </View>
    </ScrollView>
  </SafeAreaView>
);

const SecondRoute = () => (
  <SafeAreaView style={styles.container}>
    <ScrollView
      style={styles.scrollView}
      showsVerticalScrollIndicator={true}
      contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'space-between',
      }}>
      <View style={styles.page}>
        <View style={styles.header}>
          <View style={styles.title}>
            <CustomText others={styles.textTitle}>Monitoreo</CustomText>
            <Text style={{color:'black'}}>Fecha: {fecha}</Text>
            <Text style={{color:'black'}}>Semana: {formattedWeekNumber }</Text>
          </View>
        </View>
        <MonitoreoIndirecto />
      </View>
    </ScrollView>
  </SafeAreaView>
);

const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

export default function Monitoreo() {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'Monitoreo Directo' },
    { key: 'second', title: 'Monitoreo Indirecto' },
  ]);

  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.colorBlanco,
  },
  scrollView: {
    flex: 1,
  },
  page: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  header: {
    flex: 1,
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  content: {
    flex: 1,
  },
  radio: {
    flex: 2,
  },
  imgContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    maxHeight: 50,
  },
  textBody: {
    fontSize: 14,
  },
  title: {
    flex: 1,
    marginVertical: 5,
  },
  textTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    color: Colors.colorPlomo,
  },
  button: {
    backgroundColor: Colors.colorRojo,
    borderColor: Colors.colorRojo,
    borderRadius: 6,
    paddingVertical: 10,
  },
  textButton: {
    fontSize: 20,
    fontWeight: 'normal',
    color: Colors.colorBlanco,
    textAlign: 'center',
  },
  checksContainer: {
    flex: 2,
    justifyContent: 'space-evenly',
  },
  checks: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  errorText: {
    color: Colors.colorRojo,
    fontSize: 10,
  },

  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingLeft: 10,
    paddingRight: 30, //Agregamos un padding derecho para que el icono no se superponga con el texto
  },
});
const stylerows = StyleSheet.create({
  container: {
    borderWidth: 1, // Grosor del borde
    borderColor: 'red', // Color del borde
    flex: 100,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'flex-end', // Alinea los elementos en la parte superior del contenedor
  },
  gridItem: {
    width: '40%',
    margin: 4,
    justifyContent: 'center',
    alignItems: 'center',

  },
  gridItem2: {
    width: '100%',
    margin: 4,
    justifyContent: 'center',
    alignItems: 'baseline',
  },
});

const styleText = StyleSheet.create({
  text: {
    fontSize: 14,
    paddingTop: 13,
    fontWeight: 'bold',
    margin: 0,
    color: Colors.rojo1,
    paddingRight: 65,
  },
});