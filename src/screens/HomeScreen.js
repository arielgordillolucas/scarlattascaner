import React, { useState, useEffect, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { View, TextInput, Text, Button, StyleSheet, StatusBar, Alert, ActivityIndicator, ToastAndroid, PermissionsAndroid } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { FlatList, ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Formik } from 'formik';
import RNPickerSelect from 'react-native-picker-select';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CustomText from '../common/CustomText';
import Colors from '../constants/Colors';
import Icon from 'react-native-vector-icons/Ionicons';
import Geolocation from 'react-native-geolocation-service';
import { Image } from 'react-native-elements';

import { useCameraPermission, useCameraDevice, Camera, useCodeScanner } from 'react-native-vision-camera';
const fecha = new Date().toLocaleDateString('es-EC', { year: 'numeric', month: '2-digit', day: '2-digit', timeZone: 'America/Guayaquil' }).split('/').reverse().join('-');
const hora = new Date().toLocaleTimeString('es-EC', {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    timeZone: 'America/Guayaquil'
});
const HomeScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [scan, setScan] = useState(true);
    const [newR, setNewR] = useState(false);
    const [message, setMessage] = useState(null);
    const [user, setUser] = useState([]);
    const [valor, setValor] = useState('');
    const [observacion, setObservacion] = useState('');
    const [responsable, setResponsable] = useState('');
    const [personas, setPersonas] = useState('');
    const [qr, setQr] = useState(null);
    const [api, setApi] = useState(null);
    const [location, setLocation] = useState(false);
    const [globalNewOption, setGlobalNewOption] = useState('');
    const { hasPermission, requestPermission } = useCameraPermission();
    const device = useCameraDevice('back');
    // Estado para manejar la cadena de opciones seleccionadas
    const [selectedOptionsString, setSelectedOptionsString] = useState('');
    const initialRoutesState = [
        {
            id: 1,
            selectedValue: null,
            newOption: '',
            items: [
                { label: 'Chiara', value: 'Chiara', color: Colors.rojo1 },
                { label: 'Chilintosa', value: 'Chilintosa', color: Colors.rojo1 },
                { label: 'San Jose', value: 'San Jose', color: Colors.rojo1 },
                { label: 'San Luis', value: 'San Luis', color: Colors.rojo1 },
                { label: 'San Nicholas', value: 'San Nicholas', color: Colors.rojo1 },
                { label: 'San Paolo', value: 'San Paolo', color: Colors.rojo1 },
                { label: 'Mylos', value: 'Mylos', color: Colors.rojo1 },
                { label: 'Pedregal', value: 'Pedregal', color: Colors.rojo1 },
                { label: 'Santa Martha', value: 'Santa Martha', color: Colors.rojo1 },
                { label: 'San Lucca', value: 'San Lucca', color: Colors.rojo1 },
                { label: 'SierraFlor', value: 'SierraFlor', color: Colors.rojo1 },
                { label: 'Latacunga', value: 'Latacunga', color: Colors.rojo1 },
                { label: 'Quito', value: 'Quito', color: Colors.rojo1 },
            ],
        },
    ];
    const [routes, setRoutes] = useState(initialRoutesState);
    useEffect(() => {
        getLocation();

    }, [])
    if (device == null) {
        return (
            <View>
                <Text> Device no found</Text>
            </View>
        )
    }
    const handleClick = async () => {
        if (selectedOptionsString === '' || responsable === "" || observacion === '' || valor === '') {
            Alert.alert('¡ERROR!', 'Por favor, complete todos los campos para poder guardar la solicitud');
        } else {


            const res = await fetch('https://rrhh.eqrapp.com/api/v1/recorridos/postRecorrido', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {
                        ruta: selectedOptionsString ?? '',
                        id_empleado: qr.Identificacion,
                        valor: valor,
                        fecha: new Date().toLocaleDateString('es-EC', { year: 'numeric', month: '2-digit', day: '2-digit', timeZone: 'America/Guayaquil' }).split('/').reverse().join('-'),
                        hora_llegada: '',
                        hora_llegada: new Date().toLocaleTimeString('es-EC', { timeZone: 'America/Guayaquil' }),
                        lat: location !== null ? location.coords.latitude : "",
                        lon: location !== null ? location.coords.longitude : "",
                        status: 0,
                        observacion: observacion,
                        user_app: user
                    }
                ), // Enviar bodyJSON en lugar de nuevoDato
            });
            console.log("res", res);
            // Manejar la respuesta
            if (res.ok) {
                ToastAndroid.showWithGravityAndOffset(
                    'Los sobrantes ingresados se guardaron exitosamente.',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50,
                );
                setGlobalNewOption('');
                setRoutes(initialRoutesState);
                setSelectedOptionsString('');
                setQr(null);
                setObservacion('');
                setResponsable('');
                setPersonas('');
                setValor('');
            } else {
                console.log('Error al enviar la solicitud:', res.status, res.statusText, res.code);
            }

        }
    };
    const requestLocationPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Permiso de Geolocalización',
                    message: '¿Estas dispuesto a darnos accesos de tu localización?',
                    buttonNeutral: 'En otro momento',
                    buttonNegative: 'Cancelar',
                    buttonPositive: 'Aceptar',
                },
            );
            if (granted === 'granted') {
                requestPermission();

                return true;
            } else {
                return true;
            }
        } catch (err) {
            return false;
        }
    };
    const getLocation = () => {
        const result = requestLocationPermission();
        result.then(res => {
            if (res) {
                Geolocation.getCurrentPosition(
                    position => {
                        console.log("asdasd", position);
                        setLocation(position);
                    },
                    error => {
                        console.log(error.code, error.message);
                        setLocation(false);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
                );
            }
        });

    };
   const codeScanner = useCodeScanner({
        codeTypes: ['qr', 'ean-13'],
        onCodeScanned: async (codes: Code[]) => {
            try {
                console.log(codes[0].value);
                const response = await fetch('https://merizalde.scarlattasystem.com/backend/apis/appExportacionQR.php?id=' + codes[0].value.substring(2), {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                console.log(response.status);
                if (response.status === 200) {
                    const data = await response.json();
                    setQr(data);
                }else{
                    setQr(null);
                }
                // Registrar el valor del código escaneado
                console.log(`Código escaneado: ${codes[0].value}`);
            }
            catch {
                setQr(null);

                console.log(`Código no escaneado`);
            }
        }
    });

    const logout = async () => {
        AsyncStorage.removeItem('token');
        Alert.alert('¡Hasta pronto!',
            'Sesión cerrada exitosamente.');
    }
    // Función para agregar una nueva ruta
    const addRoute = () => {
        const newRoute = {
            id: routes.length + 1,
            selectedValue: null,
            newOption: '',
            items: [
                { label: 'Chiara', value: 'Chiara', color: Colors.rojo1 },
                { label: 'Chilintosa', value: 'Chilintosa', color: Colors.rojo1 },
                { label: 'San Jose', value: 'San Jose', color: Colors.rojo1 },
                { label: 'San Luis', value: 'San Luis', color: Colors.rojo1 },
                { label: 'San Nicholas', value: 'San Nicholas', color: Colors.rojo1 },
                { label: 'San Paolo', value: 'San Paolo', color: Colors.rojo1 },
                { label: 'Mylos', value: 'Mylos', color: Colors.rojo1 },
                { label: 'Pedregal', value: 'Pedregal', color: Colors.rojo1 },
                { label: 'Santa Martha', value: 'Santa Martha', color: Colors.rojo1 },
                { label: 'San Lucca', value: 'San Lucca', color: Colors.rojo1 },
                { label: 'SierraFlor', value: 'SierraFlor', color: Colors.rojo1 },
                { label: 'Latacunga', value: 'Latacunga', color: Colors.rojo1 },
                { label: 'Quito', value: 'Quito', color: Colors.rojo1 },
            ],
        };
        setRoutes([...routes, newRoute]);
    };

    const scam = () => {
        setScan(prevState => !prevState)
    };
    // Función para manejar el cambio de valor seleccionado
    const handleValueChange = (routeId, value) => {
        setRoutes((prevRoutes) =>
            prevRoutes.map((route) =>
                route.id === routeId ? { ...route, selectedValue: value } : route
            )
        );
    };

    // Función para manejar el cambio de nueva opción
    const handleNewOptionChange = (text) => {
        setGlobalNewOption(text);
    };

    // Función para agregar una nueva opción a todas las rutas
    const handleAddNewOption = () => {
        setRoutes((prevRoutes) =>
            prevRoutes.map((route) => {
                const newItem = { label: globalNewOption, value: globalNewOption.toLowerCase(), color: Colors.rojo1 };
                return { ...route, items: [...route.items, newItem], newOption: '' };
            })
        );
        setGlobalNewOption('');
    };
    const newRuta = () => {
        setNewR(prevState => !prevState);
    };
    const handleValor = (text) => {
        const isFloat = /^-?\d*(\.\d{0,2})?$/.test(text);
        if (isFloat) {
            setValor(text);
        }
    };
    const handleObservacion = (text) => {

        setObservacion(text);

    };
    const handlePersonas = (text) => {

        setPersonas(text);

    };
    const handleResponsable = (text) => {

        setResponsable(text);

    };
    // Función para generar la cadena de opciones seleccionadas
    const generateSelectedOptionsString = () => {
        const optionsString = routes.map(route => ` ${route.selectedValue || ''}`).join(' - ');
        setSelectedOptionsString(optionsString);
    };

    // Llamar a la función para generar la cadena de opciones seleccionadas cada vez que se modifiquen las rutas
    useEffect(() => {
        generateSelectedOptionsString();
    }, [routes]);
    useEffect(() => {
        const fetchToken = async () => {
            const value = await AsyncStorage.getItem('token');
            const userObject = JSON.parse(value);
            setUser(userObject[0].username);
        };
        fetchToken();
    }, []);
    const mostrarDialogo = () => {
        if (message === 200) {
            Alert.alert('Actualización realizada exitosamente',
                'El plano de cultivo ha sido actualizado correctamente.',
                [
                    {
                        text: 'Ok',
                        onPress: async () => {
                            setLoading(false);
                            setMessage(null);
                        }
                    }
                ]
            );
        }

    };

    const handleLoadData = () => {
        setLoading(true);

    };

    // Mostrar el diálogo cuando el estado de 'message' cambie
    useEffect(() => {
        mostrarDialogo();
    }, [message, api]);
    return (
        <SafeAreaView style={styles.container}>

            <ScrollView
                style={styles.scrollView}
                showsVerticalScrollIndicator={true}
                contentContainerStyle={{
                    flexGrow: 1,
                    justifyContent: 'space-between',
                }}>


                {loading ? (<ActivityIndicator size="large" color="#0000ff" style={{ paddingTop: 150 }} />) :
                    (
                    
                    <View style={styles.page}>
                        
                        <View style={styles.header}>
                            <View style={styles.title}>
                            <View style={styles.rowContainer2}>
                                    <View style={styles.column2}>
 <Image
                                            source={require('./../assets/icono.png')}
                                            style={{ width: 20, height: 40 }}
                                        />
                                        <CustomText others={styles.textTitle}>ESCANER QR </CustomText>
                                        <View style={styles.column2}>

                                            <Text style={{ color: Colors.rojo1 }}>Fecha: {fecha}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View >
                              
                                </View>
                                <Formik>
                                    {({ }) => (
                                        <>
                                            <View style={styles.routeContainer}>
                                            <View style={{ width: 'auto', height: 300, alignItems: 'center', flex: 1, padding: 10 }}>
                                                    <Camera
                                                        style={StyleSheet.absoluteFill}
                                                        device={device}
                                                        isActive={true}
                                                        codeScanner={codeScanner}
                                                    />
                                                </View> 


                                            {qr !== null ? (
                                            <>
                                            
                                            <View style={styles.routeContainer}>
                                                    <Text style={styles.label}>Producto</Text>
                                                    <TextInput
                                                        style={styles.input}  
                                                        value={qr.Producto ? qr.Producto : 'SN'} />
                                                </View>
                                                <View style={styles.routeContainer}>
                                                    <Text style={styles.label}> N° de Producto</Text>
                                                    <TextInput
                                                    
                                                        style={styles.input}  
                                                        value={qr.NroPedido ? qr.NroPedido : 0} />
                                                </View>
                                                <View style={styles.rowContainer2}>
                                                        <View style={styles.column2}>
                                                            <Text style={styles.label}>Fecha</Text>
                                                            <TextInput
                                                                style={styles.input} 
                                                                value={qr.Fecha ? qr.Fecha : 'SN'}  />
                                                        </View>
                                                        <View style={styles.column2}>
                                                            <Text style={styles.label}>Hora</Text>
                                                            <TextInput
                                                                style={styles.input} 
                                                                value={qr.Hora ? qr.Hora : 'SN'} />
                                                        </View>

                                                    </View> 
                                                     
                                                    </>
                                                )
                                                    : (
                                                    <></>
                                                    )
                                                    }
                                                    </View> 
                                                    </>
                                    )}

                                                </Formik>
                                            </View>

                                        </View>

                            </View>)
                }

                        </ScrollView>

                    </SafeAreaView>
                    );
};

                export default HomeScreen;
                const styles = StyleSheet.create({
                    button1: {
                    backgroundColor: Colors.rojo1,
                borderRadius: 5,
                padding: 10,
                marginBottom: 10,
                alignItems: 'center',
    },
                buttonText1: {
                    color: 'white',
                fontWeight: 'bold',
    },
                routeContainer: {
                    padding: 10,
    },
                rowContainer2: {
                    padding: 10,
                flexDirection: 'row',
                justifyContent: 'space-between', // Ajusta esto para distribuir el espacio entre las columnas
                alignItems: 'center', // Alinea verticalmente los elementos
                marginBottom: 5, // Añade un margen inferior si es necesario
    },
                column2: {
                    flex: 1, // Asegura que las columnas tengan el mismo ancho
                marginRight: 5, // Añade un margen entre columnas si es necesario
    },
                label: {
                    fontSize: 16,
                color: Colors.rojo1,
                fontWeight: 'bold',
    },

                input: {
                    borderWidth: 1,
                borderColor: '#ccc',
                color: '#666666',
                marginBottom: 10,
    },
                selectedValue: {
                    marginTop: 10,
                fontSize: 16,
    },
                addRouteButton: {
                    marginTop: 20,
    },
                horizontal: {
                    flexDirection: 'row',
                justifyContent: 'space-around',
                padding: 10,
    },
                container: {
                    flex: 1,
                backgroundColor: Colors.colorBlanco,
    },
                scrollView: {
                    flex: 1,
    },
                page: {
                    flex: 1,
                paddingHorizontal: 20,
                paddingVertical: 10,
    },

                footer: {
                    flex: 1,
                justifyContent: 'flex-end',
    },
                content: {
                    flex: 1,
    },
                radio: {
                    flex: 2,
    },
                imgContainer: {
                    flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                maxHeight: 50,
    },
                textBody: {
                    fontSize: 14,
                fontWeight: 'bold',
                color: Colors.rojo1,
    },
                title: {
                    flex: 1,
                marginVertical: 5,
    },
                textTitle: {
                    fontSize: 25,
                fontWeight: 'bold',
                color: Colors.rojo1,
    },
                button: {
                    backgroundColor: Colors.colorRojo,
                borderColor: Colors.colorRojo,
                borderRadius: 6,
                paddingVertical: 10,
    },
                textButton: {
                    fontSize: 20,
                fontWeight: 'normal',
                color: Colors.colorBlanco,
                textAlign: 'center',
    },
                checksContainer: {
                    flex: 2,
                justifyContent: 'space-evenly',
    },
                checks: {
                    flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
    },
                errorText: {
                    color: Colors.colorRojo,
                fontSize: 10,
    },

                input: {
                    borderWidth: 1,
                borderColor: '#ccc',
                color: '#666666',
                borderRadius: 5,
                paddingLeft: 10,
                paddingRight: 30, //Agregamos un padding derecho para que el icono no se superponga con el texto
    },
});
                const list = StyleSheet.create({
                    container: {
                    flex: 1,
                marginTop: 0, // Establece el margen superior a 0 para iniciar en la parte superior
                justifyContent: 'center',
    },
                card: {
                    backgroundColor: 'white',
                borderRadius: 8,
                borderColor: Colors.rojo1,
                padding: 10,
                marginVertical: 10,
                width: '100%',
                shadowColor: '#000',
                shadowOffset: {width: 0, height: 2 },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 5,
    },
                floatingButtonleft: {
                    position: 'absolute',
                bottom: 20,
                right: 20,
                backgroundColor: Colors.rojo1,
                borderRadius: 60,
                width: 60,
                height: 40,
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 5,
    },
                floatingButton: {
                    position: 'absolute',
                bottom: 20,
                right: 20,
                backgroundColor: Colors.colorCafeRojizo,
                borderRadius: 40,
                width: 60,
                height: 60,
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 5,
    },
                floatingButtonLeft: {
                    position: 'absolute',
                bottom: 20,
                right: 20,
                paddingRight: 30,
                backgroundColor: Colors.colorCafeRojizo,
                borderRadius: 30,
                width: 60,
                height: 60,
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 5,
    },
                floatingButtonText: {
                    color: 'white',
                fontSize: 24,
                fontWeight: 'bold',
    },
})