import React from 'react';
import { View, 
    Text, ActivityIndicator, StyleSheet, Modal, Platform } from 'react-native';
//CONSTANT
import Colors from "../constants/Colors";
//COMMONS
export default function Loader(props) {
    if(props.noFound){
        return(
            <View style={[style.noFoundContainer]} >
                   <Text>{props.message ? props.message : 'No se ha encontrado resultados'} </Text>
            </View>
        )
    }
    if(props.loaderData){
        return (
            <View 
                style={[!props.style ? style.containerLoader : props.style, props.backgroundColor && {backgroundColor: props.backgroundColor}]}
            >
                <ActivityIndicator size={props.size} color={props.color ? props.color : Colors.colorAzulAlcivar} />
            </View>
        );
    }
    if(Platform.OS === "android"){
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={props.isVisible}
                onRequestClose={() => {
                    // console.log('Modal Openned')
                    // props.setIsVisible(false);
                }}
                statusBarTranslucent={true} //ANDROID
            >
                <View style={style.containerLoader}>
                    <ActivityIndicator size={props.size} color={props.color ? props.color : Colors.colorAzulAlcivar} />
                </View>
            </Modal>
        );
    }
    if(Platform.OS === "ios"){
        return ( 
            props.isVisible ?
                    <View 
                        style={[!props.style ? style.containerLoaderIOS : props.style, props.backgroundColor && {backgroundColor: props.backgroundColor}]}
                    >
                        <ActivityIndicator size={props.size} color={props.color ? props.color : Colors.colorAzulAlcivar} />
                    </View>
            : null
        )
    }
    return null;
}

const style = StyleSheet.create({
    containerLoader:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: Colors.colorRGBAModal, 
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: 9999,
    },
    containerLoaderIOS:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: 'rgba(255,255,255,.2)', 
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: 9999,
    },
    noFoundContainer: {
        justifyContent: "center",
        alignItems: 'center',
        paddingHorizontal: 20,
        flex: 1
    },
    text: {
        color: Colors.colorNegroAlcivar,
        fontSize: 14
    }
});