export default {
  //paleta
  rojo1:"#a41105",
  rojo2: "#b32010",
  rojo3: "#C22F1B",
  rojo4: "#D03E26",
  rojo5: "#DF4D31",

  colorVerde: "#94c93f",
  colorVerdeSuave: "#d4ee9a",
  colorVerdeClaro: "#93d500",
  colorAmarrillo: "#fbc415",
  colorVioleta: "#891a47",
  colorTomate: "#ef7e2f",
  colorTomateRgba: "rgba(239,126,47, .100)",
  colorBeich: "#f5f3e6",
  colorCafeRojizo: "#842d37",
  colorBlanco: "#ffffff",
  colorRojo: "#c12033",
  colorPlomo: "#231f20",
  colorPlomoSuave: "#c2c4c6",
  colorNegro: "#000000",
  colorNaranja: "#e75025",
  colorPlomoInfo: '#b6b6b6',
  colorAzul: '#2072b9',

  //gradiente
  colorGradiente1: '#f8f8f8',
  colorGradiente2: '#f1f2f2',
  colorGradiente3: '#dfe0e1',

  colorBlancoOpaco: '#f5f6f6',
};
