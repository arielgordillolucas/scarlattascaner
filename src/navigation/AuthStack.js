import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/Login/Login';
import Colors from '../constants/Colors';
const Stack = createStackNavigator();

export default function AuthStack(props) {
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
    >
      
      <Stack.Screen
        name={'LoginScreen'}
        component={Login}
        options={{
          headerShown: false,
          headerTitle: '',
          headerStyle: {
            backgroundColor: Colors.colorBlanco,
          },
        }} />
        
    </Stack.Navigator>
  );
}
