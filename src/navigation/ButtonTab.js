import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from 'react';
import HomeScreen from "../screens/HomeScreen";
import Icon from 'react-native-vector-icons/Ionicons'; 
import Colors from "../constants/Colors";
import { StyleSheet, View } from 'react-native';
import Monitoreo from "../screens/Monitoreo";
import Login from "../screens/Login/Login";
const Tab = createBottomTabNavigator();
import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

function Home() {
  return (
    <Tab.Navigator
    initialRouteName="Home"
  activeColor="#f0edf6"
  inactiveColor="#3e2465"
  barStyle={{ paddingBottom: 48 }}
    >
      <Stack.Screen
    name="Inicio"
    component={HomeScreen}
    options={{
      headerShown: false,
      ...tabBarIconOption('home'),
    }}
  /> 
    
  {/* <Stack.Screen
    name="Control"
    component={Monitoreo}
    options={{ headerShown: false , ...tabBarIconOption('reader'),}}
  />    */}
    </Tab.Navigator>
  );
}
export const ButtonTab =  () =>{ 
  return (
    <NavigationContainer independent={true}>
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      /> 
    </Stack.Navigator>
  </NavigationContainer>
  );
}
//TAB BAR ICONS CUSTOM
function tabBarIconOption(icon, heightIcon) {
  let iconName = icon;
 
  return {
     
      tabBarIcon: ({ focused }) => (
          <View style={{ marginVertical: 0  }}>
              <Icon
                  name={iconName}
                  size={30}
                  color={focused ? Colors.rojo1 : Colors.rojo5}
                 
              />
          </View>
      ),
      
  };
}
 
const tabBarStyle = {
  activeTintColor: '#000',
  style: Platform.OS === 'android' ? {
      height: 60,
      elevation: 2,
      borderTopWidth: 1,
      //marginBottom: 2,
      backgroundColor: Colors.colorBeich,
      //top: 1, // This is the important part.. without it there will be no shadow effect on the top edge of the tab bar.
  } : {
      backgroundColor: Colors.colorBeich,
      shadowColor:'#000',
      shadowOffset:{
          width: 0,
          height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
  },
  adaptive: true,
  keyboardHidesTabBar: true,
};
