export default Users = [
    {
        id: 1, 
        email: 'user1@email.com',
        username: 'Alexandra', 
        password: 'alexandra1234', 
        userToken: 'token123'
    },
    {
        id: 2, 
        email: 'user2@email.com',
        username: 'DonLlano', 
        password: 'service2412', 
        userToken: 'token12345'
    }, 
    {
        id: 3, 
        email: 'user2@email.com',
        username: 'transportista3', 
        password: 'transportista32412', 
        userToken: 'token12345'
    }, 
    {
        id: 4, 
        email: 'user2@email.com',
        username: 'transportista4', 
        password: 'transportista32311', 
        userToken: 'token12345'
    }, 
];