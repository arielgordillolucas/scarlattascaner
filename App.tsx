import { View, Text, ActivityIndicator } from 'react-native';
import React, { useCallback, useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { initDatabase } from './src/ultis/DB';
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaProvider, SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import { AuthContext } from './src/context/AuthContext';
import AuthStack from './src/navigation/AuthStack';
import { ButtonTab } from './src/navigation/ButtonTab';
import AsyncStorage from '@react-native-async-storage/async-storage';  
import { useCameraPermission, useCameraDevice, Camera, useCodeScanner } from 'react-native-vision-camera';

const Stack = createStackNavigator(); 


const App = () => {
  const { hasPermission, requestPermission } = useCameraPermission();

  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(null);
  // useEffect(() => { 
    
  //    AsyncStorage.getItem('token').then(res => { setData(res) }); setLoading(false); 
     
  // }, [data]);
  useEffect(() => {
    requestPermission();

}, []);
  useEffect(() => {
    const intervalId = setInterval(() => {
      // Cualquier lógica que quieras ejecutar antes de cada renderizado
      AsyncStorage.getItem('token').then(res => { setData(res) }); setLoading(false); 
 
    }, 1000); // 5000 milisegundos (5 segundos)

    return () => clearInterval(intervalId); // Limpiar el intervalo al desmontar el componente
  }, [data]); 
  return (
    <SafeAreaProvider> 
        <NavigationContainer>
          
             <ButtonTab /> 
             

        </NavigationContainer> 
    </SafeAreaProvider>
  )
}

export default App;

